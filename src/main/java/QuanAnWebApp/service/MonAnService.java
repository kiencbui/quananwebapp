package QuanAnWebApp.service;

import QuanAnWebApp.dao.MonAnDao;
import QuanAnWebApp.entity.ChinhanhEntity;
import QuanAnWebApp.entity.DanhmucmonanEntity;
import QuanAnWebApp.entity.MonanEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("MonAnService")
@Transactional
public class MonAnService {

    @Autowired
    private MonAnDao dao;

    public List<MonanEntity> findMonAnByDanhMuc(DanhmucmonanEntity danhmucmonanEntity){
        return dao.findMonAnByDanhMuc(danhmucmonanEntity);
    }
    public List<MonanEntity> findAllMonAn() {
        return dao.findAllMonAn();
    }
    public List<MonanEntity> findTopNineMonAn() {
        return dao.findTopNineMonAn();
    }

    public List<MonanEntity> findNewMonAn() {
        return dao.findNewMonAn();
    }

    public MonanEntity findById(Integer id){
        return dao.getByKey(id);
    }

    public List<MonanEntity> findMonAnByChiNhanh(ChinhanhEntity chinhanhEntity) {
        return dao.findMonAnByChiNhanh(chinhanhEntity);
    }

    public List<MonanEntity> searchMonAn(String keyword) {
        return dao.searchMonAn(keyword);
    }

    public List<MonanEntity> findRelative(MonanEntity monanEntity) {
        return dao.findRelative(monanEntity);
    }
}
