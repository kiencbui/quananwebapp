package QuanAnWebApp.service;

import QuanAnWebApp.dao.DonHangDao;
import QuanAnWebApp.entity.DonhangEntity;
import QuanAnWebApp.entity.KhachhangEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Service("DonHangService")
@Transactional
public class DonHangService {

    @Autowired
    private DonHangDao dao;

    public List<DonhangEntity> findDonHangByKhachHang(KhachhangEntity khachhangEntity){
        List<DonhangEntity> donhangEntities = dao.findDonHangByKhachHang(khachhangEntity);

        Map<Integer,DonhangEntity> map = new HashMap<Integer, DonhangEntity>();

        for (DonhangEntity donHang : donhangEntities) {
            Integer key = donHang.getId();
            if(!map.containsKey(key)){
                map.put(key,donHang);
            }
        }

        List<DonhangEntity> result = new LinkedList<>();

       for(Map.Entry<Integer,DonhangEntity> entry : map.entrySet()) {
            result.add(entry.getValue());
       }

       return result;

    }

    public DonhangEntity insertDonHang(DonhangEntity donhangEntity) {
        return dao.insertDonHang(donhangEntity);
    }

    public DonhangEntity findDonHangByTime(DonhangEntity donhangEntity) {
        return dao.findDonHangByTime(donhangEntity);
    }

}
