package QuanAnWebApp.service;

import QuanAnWebApp.dao.DanhMucDao;
import QuanAnWebApp.entity.DanhmucmonanEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("DanhMucService")
@Transactional
public class DanhMucService {

    @Autowired
    private DanhMucDao dao;

    public List<DanhmucmonanEntity> findAllDanhMuc() {
        return dao.findAllDanhMuc();
    }

    public DanhmucmonanEntity getByKey(Integer key){
        return dao.getByKey(key);
    }
}
