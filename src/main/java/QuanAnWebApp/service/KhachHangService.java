package QuanAnWebApp.service;

import QuanAnWebApp.dao.KhachHangDao;
import QuanAnWebApp.entity.KhachhangEntity;
import QuanAnWebApp.model.LoginForm;
import QuanAnWebApp.model.RegisterForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("KhachHangService")
@Transactional
public class KhachHangService {
    @Autowired
    KhachHangDao dao;

    public void saveKhachHang(KhachhangEntity khachhangEntity){
        dao.saveKhachHang(khachhangEntity);
    }

    public KhachhangEntity findKhachHang(LoginForm loginForm) {
        return dao.findKhachHang(loginForm);
    }

    public KhachhangEntity findKhachHang(RegisterForm registerForm){
        return dao.findKhachHang(registerForm);
    }
}
