package QuanAnWebApp.service;

import QuanAnWebApp.dao.ChinhNhanhDao;
import QuanAnWebApp.entity.ChinhanhEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("ChinhNhanhService")
@Transactional
public class ChiNhanhService {

    @Autowired
    private ChinhNhanhDao dao;

    public List<ChinhanhEntity> findAllChiNhanh() {
        return dao.findAllChiNhanh();
    }

    public ChinhanhEntity findChinhNhanhByKey(Integer key) {
        return dao.getByKey(key);
    }
}
