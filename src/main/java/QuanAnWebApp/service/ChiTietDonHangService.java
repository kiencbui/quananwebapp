package QuanAnWebApp.service;


import QuanAnWebApp.dao.ChiTietDonHangDao;
import QuanAnWebApp.entity.ChitietdonhangEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("ChiTietDonHangService")
@Transactional
public class ChiTietDonHangService {

    @Autowired
    private ChiTietDonHangDao donHangDao;

    public void insertChiTietDonHang(ChitietdonhangEntity chitietdonhangEntity) {
        donHangDao.insertChiTietDonHang(chitietdonhangEntity);
    }
}
