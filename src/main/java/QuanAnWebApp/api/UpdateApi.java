package QuanAnWebApp.api;

import QuanAnWebApp.entity.DangkiKmEntity;
import QuanAnWebApp.entity.KhuyenMaiService;
import QuanAnWebApp.model.App;
import QuanAnWebApp.model.SubscribeForm;
import QuanAnWebApp.service.ChiNhanhService;
import QuanAnWebApp.service.DanhMucService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UpdateApi {

    @Autowired
    private DanhMucService danhMucService;

    @Autowired
    private ChiNhanhService chiNhanhService;

    @Autowired
    private KhuyenMaiService khuyenMaiService;

    @RequestMapping("/api/update/danhmuc")
    public String updateDanhMuc() {

        App.dsDanhMuc = danhMucService.findAllDanhMuc();

        return "done";
    }

    @RequestMapping("/api/update/chinhanh")
    public String updateChiNhanh() {

        App.dsChiNhanh = chiNhanhService.findAllChiNhanh();

        return "done";
    }

    @RequestMapping(value = "/api/subscribe", method = RequestMethod.POST)
    public String Subscribe(@ModelAttribute SubscribeForm form) {

        DangkiKmEntity kmEntity = new DangkiKmEntity();
        kmEntity.setMail(form.getEmail());

        khuyenMaiService.insertSubscribe(kmEntity);

        return "done";
    }
}
