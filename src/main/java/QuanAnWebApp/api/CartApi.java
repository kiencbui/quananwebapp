package QuanAnWebApp.api;

import QuanAnWebApp.entity.MonanEntity;
import QuanAnWebApp.model.AddCartForm;
import QuanAnWebApp.model.Cart;
import QuanAnWebApp.service.MonAnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
public class CartApi {

    @Autowired
    private Cart cart;

    @Autowired
    private MonAnService monAnService;

    @RequestMapping(value = "/api/addToCart", method = RequestMethod.POST)
    public String addToCart(@ModelAttribute AddCartForm form, Model model) {

        MonanEntity monanEntity = monAnService.findById(form.getIdMonAn());
        Integer soLuong = form.getSoLuong();

        HashMap<MonanEntity, Integer> map;

        if((map = cart.getDsMonAn()) == null)
            map = new HashMap<MonanEntity,Integer>();

        if(map.containsKey(monanEntity))
            map.remove(monanEntity);
        map.put(monanEntity,soLuong);

        cart.setDsMonAn(map);
        return "done";
    }


    @RequestMapping(value = "/api/removeFromCart/{id}", method = RequestMethod.POST)
    public String removeFromCart(@PathVariable("id") int id, Model model) {

        MonanEntity monanEntity = monAnService.findById(id);
        HashMap<MonanEntity, Integer> map;

        if((map = cart.getDsMonAn()) == null)
            map = new HashMap<MonanEntity,Integer>();

        if(map.containsKey(monanEntity))
            map.remove(monanEntity);

        cart.setDsMonAn(map);
        return "done";
    }
}
