package QuanAnWebApp.dao;

import QuanAnWebApp.entity.ChitietdonhangEntity;
import org.springframework.stereotype.Repository;

@Repository("ChiTietDonHangDao")
public class ChiTietDonHangDao extends AbstractDao<Integer,ChitietdonhangEntity> {

//    public void insertChiTietDonHang(List<ChitietdonhangEntity> chitietdonhangEntities) {
//        for (ChitietdonhangEntity chitietdonhangEntity: chitietdonhangEntities) {
//            persist(chitietdonhangEntity);
//        }
//    }

    public void insertChiTietDonHang(ChitietdonhangEntity chitietdonhangEntity) {
        persist(chitietdonhangEntity);
    }
}
