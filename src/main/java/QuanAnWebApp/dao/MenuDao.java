package QuanAnWebApp.dao;

import QuanAnWebApp.entity.ChinhanhEntity;
import QuanAnWebApp.entity.MenuEntity;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MenuDao extends AbstractDao<Integer, MenuEntity> {

    public List<MenuEntity> findMenuByChiNhanh(ChinhanhEntity chinhanhEntity) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("chinhanhByIdChiNhanh",chinhanhEntity));
        return (List<MenuEntity> ) criteria.list();
    }
}
