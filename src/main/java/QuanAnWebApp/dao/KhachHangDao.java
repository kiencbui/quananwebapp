package QuanAnWebApp.dao;

import QuanAnWebApp.entity.KhachhangEntity;
import QuanAnWebApp.model.LoginForm;
import QuanAnWebApp.model.RegisterForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository("KhachHangDao")
public class KhachHangDao extends AbstractDao<Integer,KhachhangEntity> {

    public void saveKhachHang(KhachhangEntity khachhangEntity) {
        persist(khachhangEntity);
    }

    @SuppressWarnings("unchecked")
    public KhachhangEntity findKhachHang(LoginForm loginForm) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("taiKhoan",loginForm.getUsername()));
        criteria.add(Restrictions.eq("matKhau",loginForm.getPassword()));

        return (KhachhangEntity) criteria.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    public KhachhangEntity findKhachHang(RegisterForm registerForm) {

        Criteria criteria = createEntityCriteria();

        Criterion rest1 = Restrictions.eq("taiKhoan",registerForm.getTaiKhoan());
        Criterion rest2 = Restrictions.eq("sdt",registerForm.getSdt());
        Criterion rest3 = Restrictions.eq("email",registerForm.getEmail());

        criteria.add(Restrictions.or(rest1,rest2,rest3));

        return (KhachhangEntity) criteria.uniqueResult();
    }
}
