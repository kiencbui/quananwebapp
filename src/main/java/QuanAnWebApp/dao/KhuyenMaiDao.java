package QuanAnWebApp.dao;

import QuanAnWebApp.entity.DangkiKmEntity;
import org.springframework.stereotype.Repository;

@Repository("KhuyenMaiDao")
public class KhuyenMaiDao extends AbstractDao<Integer,DangkiKmEntity> {

    public void insertSubscribe(DangkiKmEntity km) {
        persist(km);
    }
}
