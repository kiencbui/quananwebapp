package QuanAnWebApp.dao;

import QuanAnWebApp.entity.ChinhanhEntity;
import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("ChiNhanhDao")
public class ChinhNhanhDao extends AbstractDao<Integer,ChinhanhEntity> {
    @SuppressWarnings("unchecked")
    public List<ChinhanhEntity> findAllChiNhanh() {
        Criteria criteria = createEntityCriteria();
        return (List<ChinhanhEntity>) criteria.list();
    }

    @Override
    public ChinhanhEntity getByKey(Integer key) {
        return super.getByKey(key);
    }
}
