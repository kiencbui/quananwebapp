package QuanAnWebApp.dao;

import QuanAnWebApp.entity.DanhmucmonanEntity;
import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("DanhMucDao")
public class DanhMucDao extends AbstractDao<Integer,DanhmucmonanEntity> {

    @SuppressWarnings("unchecked")
    public List<DanhmucmonanEntity> findAllDanhMuc() {
        Criteria criteria = createEntityCriteria();
        return (List<DanhmucmonanEntity>) criteria.list();
    }

    @Override
    public DanhmucmonanEntity getByKey(Integer key) {
        return super.getByKey(key);
    }
}
