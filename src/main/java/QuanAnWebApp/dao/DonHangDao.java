package QuanAnWebApp.dao;

import QuanAnWebApp.entity.DonhangEntity;
import QuanAnWebApp.entity.KhachhangEntity;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("DonHangDao")
public class DonHangDao extends AbstractDao<Integer,DonhangEntity>{

    public List<DonhangEntity> findDonHangByKhachHang(KhachhangEntity khachhangEntity) {

        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("khachhangByIdKhachHang",khachhangEntity));

        return (List<DonhangEntity>)criteria.list();
    }

    public DonhangEntity insertDonHang(DonhangEntity donhangEntity) {
        persist(donhangEntity);
        return donhangEntity;
    }

    public DonhangEntity findDonHangByTime(DonhangEntity donhangEntity) {
        Criteria criteria = createEntityCriteria();

        criteria.addOrder(Order.desc("id"));
        criteria.add(Restrictions.eq("diaChiGiaoHang",donhangEntity.getDiaChiGiaoHang()));
        criteria.add(Restrictions.eq("khachhangByIdKhachHang",donhangEntity.getKhachhangByIdKhachHang()));

        return (DonhangEntity) criteria.uniqueResult();
    }
}
