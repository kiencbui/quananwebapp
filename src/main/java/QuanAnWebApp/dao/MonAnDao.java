package QuanAnWebApp.dao;

import QuanAnWebApp.entity.ChinhanhEntity;
import QuanAnWebApp.entity.DanhmucmonanEntity;
import QuanAnWebApp.entity.MenuEntity;
import QuanAnWebApp.entity.MonanEntity;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.LinkedList;
import java.util.List;

@Repository("MonAnDao")
public class MonAnDao extends AbstractDao<Integer,MonanEntity> {

    @SuppressWarnings("unchecked")
    public List<MonanEntity> findTopNineMonAn() {

        Criteria criteria = createEntityCriteria();
        criteria.setMaxResults(3);
        return (List<MonanEntity>) criteria.list();
    }

    @SuppressWarnings("unchecked")
    public List<MonanEntity> findNewMonAn() {
        Criteria criteria = createEntityCriteria();
        criteria.addOrder(Order.desc("id"));
        criteria.setMaxResults(3);
        return (List<MonanEntity>) criteria.list();
    }

    @SuppressWarnings("unchecked")
    public List<MonanEntity> findAllMonAn() {
        Criteria criteria = createEntityCriteria();
        return (List<MonanEntity>) criteria.list();
    }

    @SuppressWarnings("unchecked")
    public List<MonanEntity> findMonAnByDanhMuc(DanhmucmonanEntity danhmucmonanEntity) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("danhmucmonanByIdDanhMuc",danhmucmonanEntity));
        return (List<MonanEntity>) criteria.list();
    }

    @SuppressWarnings("unchecked")
    public MonanEntity getByKey(Integer key) {
        return super.getByKey(key);
    }

    @SuppressWarnings("unchecked")
    public List<MonanEntity> findMonAnByChiNhanh(ChinhanhEntity chinhanhEntity) {
        MenuDao menuDao = new MenuDao();
        List<MenuEntity> menuEntityList = menuDao.findMenuByChiNhanh(chinhanhEntity);
        List<MonanEntity> monanEntities = new LinkedList<>();
        for (MenuEntity menu : menuEntityList) {
            monanEntities.add(menu.getMonanByIdMonAn());
        }
        return monanEntities;
    }

    @SuppressWarnings("unchecked")
    public List<MonanEntity> searchMonAn(String keyword) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.like("ten","%"+keyword+"%"));
        return (List<MonanEntity>) criteria.list();
    }

    @SuppressWarnings("unchecked")
    public List<MonanEntity> findRelative(MonanEntity monanEntity) {
        List<MonanEntity> list = findMonAnByDanhMuc(monanEntity.getDanhmucmonanByIdDanhMuc());
        return list.size() <=5? list: list.subList(0,4);
    }

}
