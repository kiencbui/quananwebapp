package QuanAnWebApp.controller;

import QuanAnWebApp.entity.ChitietdonhangEntity;
import QuanAnWebApp.entity.DonhangEntity;
import QuanAnWebApp.entity.MonanEntity;
import QuanAnWebApp.model.Cart;
import QuanAnWebApp.model.CartItem;
import QuanAnWebApp.model.CheckoutForm;
import QuanAnWebApp.model.UserLogin;
import QuanAnWebApp.service.ChiTietDonHangService;
import QuanAnWebApp.service.DonHangService;
import QuanAnWebApp.service.MonAnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

@Controller
public class CartController {

    @Autowired
    private UserLogin userLogin;

    @Autowired
    private Cart cart;

    @Autowired
    private MonAnService monAnService;

    @Autowired
    private DonHangService donHangService;

    @Autowired
    private ChiTietDonHangService chiTietDonHangService;

    @RequestMapping("/cart")
    public String showCart(Model model){
        List<CartItem> dsMonAn = new LinkedList<CartItem>();

        HashMap<MonanEntity, Integer> map = cart.getDsMonAn();

        for(MonanEntity monanEntity : map.keySet()){
            CartItem cartItem = new CartItem();
            cartItem.setMonAn(monanEntity);
            cartItem.setSoLuong(map.get(monanEntity));

            dsMonAn.add(cartItem);
        }

        model.addAttribute("dsMonAn", dsMonAn);
        return "cartPage";
    }

    @RequestMapping("/cart/remove/{id}")
    public String removeMonAnFromCart(@PathVariable("id") Integer id, Model model){

        MonanEntity monanEntity = monAnService.findById(id);
        HashMap<MonanEntity, Integer> map;

        if((map = cart.getDsMonAn()) == null)
            map = new HashMap<MonanEntity,Integer>();

        if(map.containsKey(monanEntity))
            map.remove(monanEntity);

        cart.setDsMonAn(map);

        return "redirect:" + "/cart";
    }

    @RequestMapping("/checkout")
    public String checkout(@ModelAttribute CheckoutForm checkoutForm, Model model) {

        if(userLogin.getUserLogin()==null){
            return "redirect:" + "/login";
        }

        DonhangEntity donhangEntity = new DonhangEntity();
        donhangEntity.setKhachhangByIdKhachHang(userLogin.getUserLogin());
        donhangEntity.setDiaChiGiaoHang(checkoutForm.getAddress());
        donhangEntity.setNgayLap(new Timestamp(System.currentTimeMillis()));
        donhangEntity.setLoaiDonHang(2);
        donhangEntity.setTinhTrang(2);

        DonhangEntity donhang = donHangService.insertDonHang(donhangEntity);

        userLogin.setDonhangEntity(donhang);

        userLogin.setCheckoutForm(checkoutForm);

        return "redirect:" + "/redirectCheckout";
    }

    @RequestMapping("/redirectCheckout")
    public String redirectCheckout() {
        List<ChitietdonhangEntity> chitietdonhangEntities = new LinkedList<>();

        DonhangEntity donHang = donHangService.findDonHangByTime(userLogin.getDonhangEntity());

        HashMap<MonanEntity,Integer> map = cart.getDsMonAn();
        for(MonanEntity monanEntity: map.keySet()) {
            ChitietdonhangEntity chitietdonhangEntity = new ChitietdonhangEntity();
            chitietdonhangEntity.setDonhangByIdDonHang(donHangService.findDonHangByTime(donHang));
            chitietdonhangEntity.setMonanByIdMonAn(monanEntity);
            chitietdonhangEntity.setSoLuong(map.get(monanEntity));
            chitietdonhangEntity.setGhiChu(userLogin.getCheckoutForm().getNote());

            chitietdonhangEntities.add(chitietdonhangEntity);
        }

//        chiTietDonHangService.insertChiTietDonHang(chitietdonhangEntities);

        for(ChitietdonhangEntity chitietdonhangEntity : chitietdonhangEntities) {
            chiTietDonHangService.insertChiTietDonHang(chitietdonhangEntity);
        }


        return "redirect:" + "/account";
    }
}
