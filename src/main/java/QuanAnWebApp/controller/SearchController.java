package QuanAnWebApp.controller;

import QuanAnWebApp.entity.MonanEntity;
import QuanAnWebApp.service.MonAnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/search")
public class SearchController {

    @Autowired
    MonAnService monAnService;

    @RequestMapping("/")
    public String showSearchPage(Model model) {
        return "searchPage";
    }

    @RequestMapping("/{keyword}")
    public String searchMonAn(Model model, @PathVariable("keyword") String keyword) {

        List<MonanEntity> dsMonAn = monAnService.searchMonAn(keyword);

        model.addAttribute("dsMonAn",dsMonAn);

        return "searchPage";
    }

}
