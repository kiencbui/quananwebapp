package QuanAnWebApp.controller;

import QuanAnWebApp.entity.ChinhanhEntity;
import QuanAnWebApp.entity.MonanEntity;
import QuanAnWebApp.service.ChiNhanhService;
import QuanAnWebApp.service.MonAnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/branch")
public class ChiNhanhController {

    @Autowired
    ChiNhanhService chiNhanhService;

    @Autowired
    MonAnService monAnService;

    @RequestMapping("/")
    public String showChiNhanhPage(Model model) {

        List<MonanEntity> dsMonAn = monAnService.findAllMonAn();
        model.addAttribute("dsMonAn",dsMonAn);

        return "chiNhanhPage";
    }

    @RequestMapping("/{id}")
    public String showChiNhanhById(Model model, @PathVariable("id") int id){

        ChinhanhEntity chinhanhEntity = chiNhanhService.findChinhNhanhByKey(id);

        if(chinhanhEntity==null){
            return "notFound";
        }
        model.addAttribute("dsMonAn",monAnService.findMonAnByChiNhanh(chinhanhEntity));

        return "chiNhanhPage";
    }
}
