package QuanAnWebApp.controller;

import QuanAnWebApp.entity.DanhmucmonanEntity;
import QuanAnWebApp.entity.MonanEntity;
import QuanAnWebApp.service.DanhMucService;
import QuanAnWebApp.service.MonAnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/category")
public class DanhMucController {

    @Autowired
    DanhMucService danhMucService;

    @Autowired
    MonAnService monAnService;

    @RequestMapping("/")
    public String showDanhMucPage(Model model) {

        List<MonanEntity> dsMonAn = monAnService.findAllMonAn();
        model.addAttribute("dsMonAn",dsMonAn);

        return "danhMucPage";
    }

    @RequestMapping("/{id}")
    public String showDanhMucById(Model model, @PathVariable("id") int id) {

        DanhmucmonanEntity danhmucmonanEntity = danhMucService.getByKey(id);

        if(danhmucmonanEntity==null){
            return "notFound";
        }
        model.addAttribute("dsMonAn",monAnService.findMonAnByDanhMuc(danhmucmonanEntity));

        return "danhMucPage";
    }

}
