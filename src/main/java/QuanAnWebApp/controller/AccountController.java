package QuanAnWebApp.controller;

import QuanAnWebApp.entity.DonhangEntity;
import QuanAnWebApp.entity.KhachhangEntity;
import QuanAnWebApp.model.LoginForm;
import QuanAnWebApp.model.RegisterForm;
import QuanAnWebApp.model.UserLogin;
import QuanAnWebApp.service.DonHangService;
import QuanAnWebApp.service.KhachHangService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.sql.Timestamp;
import java.util.List;

@Controller
public class AccountController {

    @Autowired
    KhachHangService khachHangService;

    @Autowired
    DonHangService donHangService;

    @Autowired
    private UserLogin userLogin;

    @RequestMapping("/register")
    public String showRegister(Model model) {
        if(userLogin.getUserLogin()==null)
            return "registerPage";
        else
            return "redirect:" + "/";
    }

    @RequestMapping(value = "register",method = RequestMethod.POST)
    public String validateRegister(@ModelAttribute RegisterForm registerForm, Model model) {

        if(registerForm.getTaiKhoan().equals("") ||
                registerForm.getMatKhau().equals("") ||
                registerForm.getTenKhachHang().equals("") ||
                registerForm.getSdt().equals("") ||
                registerForm.getEmail().equals("") ||
                registerForm.getDiaChi().equals("")) {
            model.addAttribute("error", "Invalid field");
            return "registerPage";
        }

        KhachhangEntity khachhangEntity = khachHangService.findKhachHang(registerForm);

        if(khachhangEntity == null) {
            KhachhangEntity kh = new KhachhangEntity();
            kh.setTaiKhoan(registerForm.getTaiKhoan());
            kh.setMatKhau(registerForm.getMatKhau());
            kh.setTenKhachHang(registerForm.getTenKhachHang());
            kh.setSdt(registerForm.getSdt());
            kh.setEmail(registerForm.getEmail());
            kh.setDiaChi(registerForm.getDiaChi());
            kh.setNgayTao(new Timestamp(System.currentTimeMillis()));
            khachHangService.saveKhachHang(kh);
            return "redirect:" + "/login";
        }
        else {
            model.addAttribute("error", "Username, phone or email exists");
            return "registerPage";
        }
    }

    @RequestMapping("/login")
    public String showLogin(Model model) {
        if(userLogin.getUserLogin() == null)
            return "loginPage";
        else
            return "redirect:" + "/";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String validateLogin(@ModelAttribute LoginForm login, Model model) {

        KhachhangEntity khachhangEntity = khachHangService.findKhachHang(login);

        if(khachhangEntity == null) {
            model.addAttribute("error","Username or password is incorrect!");
            return "loginPage";
        }
        else {
            userLogin.setUserLogin(khachhangEntity);
            return "redirect:" + "/";
        }

    }

    @RequestMapping("/logout")
    public String logout() {
        userLogin.setUserLogin(null);
        return "redirect:" + "/";
    }

    @RequestMapping("/account")
    public String showAccount(Model model) {

        if(userLogin.getUserLogin()==null){
            return "redirect:" + "/login";
        }

        List<DonhangEntity> donhangEntities = donHangService.findDonHangByKhachHang(userLogin.getUserLogin());
        model.addAttribute("dsDonHang",donhangEntities);

        return "accountPage";
    }

}
