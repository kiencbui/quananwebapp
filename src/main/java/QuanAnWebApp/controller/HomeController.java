package QuanAnWebApp.controller;

import QuanAnWebApp.entity.MonanEntity;
import QuanAnWebApp.service.MonAnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class HomeController {

    @Autowired
    private MonAnService service;

	@RequestMapping("/")
    public String showHomepage(Model model) {

        List<MonanEntity> topMonAn = service.findTopNineMonAn();
        List<MonanEntity> newMonAn = service.findNewMonAn();

        model.addAttribute("topMonAn",topMonAn);
        model.addAttribute("newMonAn",newMonAn);
        
        return "homePage";
    }
}
