package QuanAnWebApp.controller;

import QuanAnWebApp.entity.MonanEntity;
import QuanAnWebApp.service.MonAnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/detail")
public class DetailController {

    @Autowired
    MonAnService service;

    @RequestMapping("/{id}")
    public String showDetail(Model model, @PathVariable("id") int id){
        MonanEntity monanEntity = service.findById(id);
        List<MonanEntity> relative = service.findRelative(monanEntity);

        model.addAttribute("monAn",monanEntity);
        model.addAttribute("relative",relative);
        return "detailPage";
    }
}
