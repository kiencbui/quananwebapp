package QuanAnWebApp.entity;

import javax.persistence.*;

@Entity
@Table(name = "menu", schema = "quanlycuahang", catalog = "")
public class MenuEntity {
    private int id;
    private ChinhanhEntity chinhanhByIdChiNhanh;
    private MonanEntity monanByIdMonAn;

    @Id
    @Column(name = "ID", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MenuEntity that = (MenuEntity) o;

        if (id != that.id) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @ManyToOne
    @JoinColumn(name = "ID_ChiNhanh", referencedColumnName = "ID")
    public ChinhanhEntity getChinhanhByIdChiNhanh() {
        return chinhanhByIdChiNhanh;
    }

    public void setChinhanhByIdChiNhanh(ChinhanhEntity chinhanhByIdChiNhanh) {
        this.chinhanhByIdChiNhanh = chinhanhByIdChiNhanh;
    }

    @ManyToOne
    @JoinColumn(name = "ID_MonAn", referencedColumnName = "ID")
    public MonanEntity getMonanByIdMonAn() {
        return monanByIdMonAn;
    }

    public void setMonanByIdMonAn(MonanEntity monanByIdMonAn) {
        this.monanByIdMonAn = monanByIdMonAn;
    }
}
