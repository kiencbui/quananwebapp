package QuanAnWebApp.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;

@Entity
@Table(name = "khachhang", schema = "quanlycuahang", catalog = "")
public class KhachhangEntity {
    private int id;
    private String taiKhoan;
    private String matKhau;
    private String tenKhachHang;
    private String sdt;
    private String email;
    private String diaChi;
    private Timestamp ngayTao;
    private Collection<DonhangEntity> donhangsById;
    private Collection<GiohangEntity> giohangsById;

    @Id
    @Column(name = "ID", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "TaiKhoan", nullable = true, length = 50)
    public String getTaiKhoan() {
        return taiKhoan;
    }

    public void setTaiKhoan(String taiKhoan) {
        this.taiKhoan = taiKhoan;
    }

    @Basic
    @Column(name = "MatKhau", nullable = true, length = 255)
    public String getMatKhau() {
        return matKhau;
    }

    public void setMatKhau(String matKhau) {
        this.matKhau = matKhau;
    }

    @Basic
    @Column(name = "TenKhachHang", nullable = true, length = 100)
    public String getTenKhachHang() {
        return tenKhachHang;
    }

    public void setTenKhachHang(String tenKhachHang) {
        this.tenKhachHang = tenKhachHang;
    }

    @Basic
    @Column(name = "SDT", nullable = true, length = 20)
    public String getSdt() {
        return sdt;
    }

    public void setSdt(String sdt) {
        this.sdt = sdt;
    }

    @Basic
    @Column(name = "Email", nullable = true, length = 100)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "DiaChi", nullable = true, length = 300)
    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    @Basic
    @Column(name = "NgayTao", nullable = true)
    public Timestamp getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(Timestamp ngayTao) {
        this.ngayTao = ngayTao;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        KhachhangEntity that = (KhachhangEntity) o;

        if (id != that.id) return false;
        if (taiKhoan != null ? !taiKhoan.equals(that.taiKhoan) : that.taiKhoan != null) return false;
        if (matKhau != null ? !matKhau.equals(that.matKhau) : that.matKhau != null) return false;
        if (tenKhachHang != null ? !tenKhachHang.equals(that.tenKhachHang) : that.tenKhachHang != null) return false;
        if (sdt != null ? !sdt.equals(that.sdt) : that.sdt != null) return false;
        if (email != null ? !email.equals(that.email) : that.email != null) return false;
        if (diaChi != null ? !diaChi.equals(that.diaChi) : that.diaChi != null) return false;
        if (ngayTao != null ? !ngayTao.equals(that.ngayTao) : that.ngayTao != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (taiKhoan != null ? taiKhoan.hashCode() : 0);
        result = 31 * result + (matKhau != null ? matKhau.hashCode() : 0);
        result = 31 * result + (tenKhachHang != null ? tenKhachHang.hashCode() : 0);
        result = 31 * result + (sdt != null ? sdt.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (diaChi != null ? diaChi.hashCode() : 0);
        result = 31 * result + (ngayTao != null ? ngayTao.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "khachhangByIdKhachHang")
    public Collection<DonhangEntity> getDonhangsById() {
        return donhangsById;
    }

    public void setDonhangsById(Collection<DonhangEntity> donhangsById) {
        this.donhangsById = donhangsById;
    }

    @OneToMany(mappedBy = "khachhangByIdKhachHang")
    public Collection<GiohangEntity> getGiohangsById() {
        return giohangsById;
    }

    public void setGiohangsById(Collection<GiohangEntity> giohangsById) {
        this.giohangsById = giohangsById;
    }
}
