package QuanAnWebApp.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;

@Entity
@Table(name = "donhang", schema = "quanlycuahang", catalog = "")
public class DonhangEntity {
    private int id;
    private String diaChiGiaoHang;
    private Timestamp ngayLap;
    private Integer loaiDonHang;
    private Integer tinhTrang;
    private Collection<ChitietdonhangEntity> chitietdonhangsById;
    private KhachhangEntity khachhangByIdKhachHang;
    private ChinhanhEntity chinhanhByIdChiNhanh;

    @Id
    @Column(name = "ID", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "DiaChiGiaoHang", nullable = true, length = 300)
    public String getDiaChiGiaoHang() {
        return diaChiGiaoHang;
    }

    public void setDiaChiGiaoHang(String diaChiGiaoHang) {
        this.diaChiGiaoHang = diaChiGiaoHang;
    }

    @Basic
    @Column(name = "NgayLap", nullable = true)
    public Timestamp getNgayLap() {
        return ngayLap;
    }

    public void setNgayLap(Timestamp ngayLap) {
        this.ngayLap = ngayLap;
    }

    @Basic
    @Column(name = "LoaiDonHang", nullable = true, length = 50)
    public Integer getLoaiDonHang() {
        return loaiDonHang;
    }

    public void setLoaiDonHang(Integer loaiDonHang) {
        this.loaiDonHang = loaiDonHang;
    }

    @Basic
    @Column(name = "TinhTrang", nullable = true)
    public Integer getTinhTrang() {
        return tinhTrang;
    }

    public void setTinhTrang(Integer tinhTrang) {
        this.tinhTrang = tinhTrang;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DonhangEntity that = (DonhangEntity) o;

        if (id != that.id) return false;
        if (diaChiGiaoHang != null ? !diaChiGiaoHang.equals(that.diaChiGiaoHang) : that.diaChiGiaoHang != null)
            return false;
        if (ngayLap != null ? !ngayLap.equals(that.ngayLap) : that.ngayLap != null) return false;
        if (loaiDonHang != null ? !loaiDonHang.equals(that.loaiDonHang) : that.loaiDonHang != null) return false;
        if (tinhTrang != null ? !tinhTrang.equals(that.tinhTrang) : that.tinhTrang != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (diaChiGiaoHang != null ? diaChiGiaoHang.hashCode() : 0);
        result = 31 * result + (ngayLap != null ? ngayLap.hashCode() : 0);
        result = 31 * result + (loaiDonHang != null ? loaiDonHang.hashCode() : 0);
        result = 31 * result + (tinhTrang != null ? tinhTrang.hashCode() : 0);
        return result;
    }

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "donhangByIdDonHang")
    public Collection<ChitietdonhangEntity> getChitietdonhangsById() {
        return chitietdonhangsById;
    }

    public void setChitietdonhangsById(Collection<ChitietdonhangEntity> chitietdonhangsById) {
        this.chitietdonhangsById = chitietdonhangsById;
    }

    @ManyToOne
    @JoinColumn(name = "ID_KhachHang", referencedColumnName = "ID")
    public KhachhangEntity getKhachhangByIdKhachHang() {
        return khachhangByIdKhachHang;
    }

    public void setKhachhangByIdKhachHang(KhachhangEntity khachhangByIdKhachHang) {
        this.khachhangByIdKhachHang = khachhangByIdKhachHang;
    }

    @ManyToOne
    @JoinColumn(name = "ID_ChiNhanh", referencedColumnName = "ID")
    public ChinhanhEntity getChinhanhByIdChiNhanh() {
        return chinhanhByIdChiNhanh;
    }

    public void setChinhanhByIdChiNhanh(ChinhanhEntity chinhanhByIdChiNhanh) {
        this.chinhanhByIdChiNhanh = chinhanhByIdChiNhanh;
    }
}
