package QuanAnWebApp.entity;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "chiphi", schema = "quanlycuahang", catalog = "")
public class ChiphiEntity {
    private int id;
    private Timestamp ngay;
    private Integer loaiChiPhi;
    private Integer chiPhi;
    private String noiDungChi;
    private ChinhanhEntity chinhanhByIdChiNhanh;

    @Id
    @Column(name = "ID", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "Ngay", nullable = true)
    public Timestamp getNgay() {
        return ngay;
    }

    public void setNgay(Timestamp ngay) {
        this.ngay = ngay;
    }

    @Basic
    @Column(name = "LoaiChiPhi", nullable = true)
    public Integer getLoaiChiPhi() {
        return loaiChiPhi;
    }

    public void setLoaiChiPhi(Integer loaiChiPhi) {
        this.loaiChiPhi = loaiChiPhi;
    }

    @Basic
    @Column(name = "ChiPhi", nullable = true)
    public Integer getChiPhi() {
        return chiPhi;
    }

    public void setChiPhi(Integer chiPhi) {
        this.chiPhi = chiPhi;
    }

    @Basic
    @Column(name = "NoiDungChi", nullable = true, length = 100)
    public String getNoiDungChi() {
        return noiDungChi;
    }

    public void setNoiDungChi(String noiDungChi) {
        this.noiDungChi = noiDungChi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChiphiEntity that = (ChiphiEntity) o;

        if (id != that.id) return false;
        if (ngay != null ? !ngay.equals(that.ngay) : that.ngay != null) return false;
        if (loaiChiPhi != null ? !loaiChiPhi.equals(that.loaiChiPhi) : that.loaiChiPhi != null) return false;
        if (chiPhi != null ? !chiPhi.equals(that.chiPhi) : that.chiPhi != null) return false;
        if (noiDungChi != null ? !noiDungChi.equals(that.noiDungChi) : that.noiDungChi != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (ngay != null ? ngay.hashCode() : 0);
        result = 31 * result + (loaiChiPhi != null ? loaiChiPhi.hashCode() : 0);
        result = 31 * result + (chiPhi != null ? chiPhi.hashCode() : 0);
        result = 31 * result + (noiDungChi != null ? noiDungChi.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "ID_ChiNhanh", referencedColumnName = "ID")
    public ChinhanhEntity getChinhanhByIdChiNhanh() {
        return chinhanhByIdChiNhanh;
    }

    public void setChinhanhByIdChiNhanh(ChinhanhEntity chinhanhByIdChiNhanh) {
        this.chinhanhByIdChiNhanh = chinhanhByIdChiNhanh;
    }
}
