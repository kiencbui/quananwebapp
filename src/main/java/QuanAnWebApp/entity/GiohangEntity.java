package QuanAnWebApp.entity;

import javax.persistence.*;

@Entity
@Table(name = "giohang", schema = "quanlycuahang", catalog = "")
public class GiohangEntity {
    private int id;
    private KhachhangEntity khachhangByIdKhachHang;
    private MonanEntity monanByIdMonAn;

    @Id
    @Column(name = "ID", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GiohangEntity that = (GiohangEntity) o;

        if (id != that.id) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @ManyToOne
    @JoinColumn(name = "ID_KhachHang", referencedColumnName = "ID")
    public KhachhangEntity getKhachhangByIdKhachHang() {
        return khachhangByIdKhachHang;
    }

    public void setKhachhangByIdKhachHang(KhachhangEntity khachhangByIdKhachHang) {
        this.khachhangByIdKhachHang = khachhangByIdKhachHang;
    }

    @ManyToOne
    @JoinColumn(name = "ID_MonAn", referencedColumnName = "ID")
    public MonanEntity getMonanByIdMonAn() {
        return monanByIdMonAn;
    }

    public void setMonanByIdMonAn(MonanEntity monanByIdMonAn) {
        this.monanByIdMonAn = monanByIdMonAn;
    }
}
