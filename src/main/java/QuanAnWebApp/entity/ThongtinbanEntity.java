package QuanAnWebApp.entity;

import javax.persistence.*;

@Entity
@Table(name = "thongtinban", schema = "quanlycuahang", catalog = "")
public class ThongtinbanEntity {
    private int id;
    private ChinhanhEntity chinhanhByIdChiNhanh;
    private TinhtrangEntity tinhtrangByIdTinhTrangBan;

    @Id
    @Column(name = "ID", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ThongtinbanEntity that = (ThongtinbanEntity) o;

        if (id != that.id) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @ManyToOne
    @JoinColumn(name = "ID_ChiNhanh", referencedColumnName = "ID")
    public ChinhanhEntity getChinhanhByIdChiNhanh() {
        return chinhanhByIdChiNhanh;
    }

    public void setChinhanhByIdChiNhanh(ChinhanhEntity chinhanhByIdChiNhanh) {
        this.chinhanhByIdChiNhanh = chinhanhByIdChiNhanh;
    }

    @ManyToOne
    @JoinColumn(name = "ID_TinhTrangBan", referencedColumnName = "ID")
    public TinhtrangEntity getTinhtrangByIdTinhTrangBan() {
        return tinhtrangByIdTinhTrangBan;
    }

    public void setTinhtrangByIdTinhTrangBan(TinhtrangEntity tinhtrangByIdTinhTrangBan) {
        this.tinhtrangByIdTinhTrangBan = tinhtrangByIdTinhTrangBan;
    }
}
