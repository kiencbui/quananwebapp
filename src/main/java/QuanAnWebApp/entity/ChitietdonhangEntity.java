package QuanAnWebApp.entity;

import javax.persistence.*;

@Entity
@Table(name = "chitietdonhang", schema = "quanlycuahang", catalog = "")
public class ChitietdonhangEntity {
    private int id;
    private Integer soLuong;
    private String ghiChu;
    private DonhangEntity donhangByIdDonHang;
    private MonanEntity monanByIdMonAn;

    @Id
    @Column(name = "ID", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "SoLuong", nullable = true)
    public Integer getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(Integer soLuong) {
        this.soLuong = soLuong;
    }

    @Basic
    @Column(name = "GhiChu", nullable = true, length = 100)
    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChitietdonhangEntity that = (ChitietdonhangEntity) o;

        if (id != that.id) return false;
        if (soLuong != null ? !soLuong.equals(that.soLuong) : that.soLuong != null) return false;
        if (ghiChu != null ? !ghiChu.equals(that.ghiChu) : that.ghiChu != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (soLuong != null ? soLuong.hashCode() : 0);
        result = 31 * result + (ghiChu != null ? ghiChu.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "ID_DonHang", referencedColumnName = "ID")
    public DonhangEntity getDonhangByIdDonHang() {
        return donhangByIdDonHang;
    }

    public void setDonhangByIdDonHang(DonhangEntity donhangByIdDonHang) {
        this.donhangByIdDonHang = donhangByIdDonHang;
    }

    @ManyToOne
    @JoinColumn(name = "ID_MonAn", referencedColumnName = "ID")
    public MonanEntity getMonanByIdMonAn() {
        return monanByIdMonAn;
    }

    public void setMonanByIdMonAn(MonanEntity monanByIdMonAn) {
        this.monanByIdMonAn = monanByIdMonAn;
    }
}
