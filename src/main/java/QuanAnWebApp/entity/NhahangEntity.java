package QuanAnWebApp.entity;

import javax.persistence.*;

@Entity
@Table(name = "nhahang", schema = "quanlycuahang", catalog = "")
public class NhahangEntity {
    private int id;
    private String ten;
    private String diaChi;
    private String sdt;
    private String thongTinKhach;

    @Id
    @Column(name = "ID", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "Ten", nullable = true, length = 100)
    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    @Basic
    @Column(name = "DiaChi", nullable = true, length = 100)
    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    @Basic
    @Column(name = "SDT", nullable = true, length = 15)
    public String getSdt() {
        return sdt;
    }

    public void setSdt(String sdt) {
        this.sdt = sdt;
    }

    @Basic
    @Column(name = "ThongTinKhach", nullable = true, length = 100)
    public String getThongTinKhach() {
        return thongTinKhach;
    }

    public void setThongTinKhach(String thongTinKhach) {
        this.thongTinKhach = thongTinKhach;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NhahangEntity that = (NhahangEntity) o;

        if (id != that.id) return false;
        if (ten != null ? !ten.equals(that.ten) : that.ten != null) return false;
        if (diaChi != null ? !diaChi.equals(that.diaChi) : that.diaChi != null) return false;
        if (sdt != null ? !sdt.equals(that.sdt) : that.sdt != null) return false;
        if (thongTinKhach != null ? !thongTinKhach.equals(that.thongTinKhach) : that.thongTinKhach != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (ten != null ? ten.hashCode() : 0);
        result = 31 * result + (diaChi != null ? diaChi.hashCode() : 0);
        result = 31 * result + (sdt != null ? sdt.hashCode() : 0);
        result = 31 * result + (thongTinKhach != null ? thongTinKhach.hashCode() : 0);
        return result;
    }
}
