package QuanAnWebApp.entity;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "chinhanh", schema = "quanlycuahang", catalog = "")
public class ChinhanhEntity {
    private int id;
    private String ten;
    private String diaChi;
    private String sdt;
    private String tinhThanh;
    private Integer soLuongBan;
    private Integer idQuanLy;
    private Collection<ChiphiEntity> chiphisById;
    private Collection<DonhangEntity> donhangsById;
    private Collection<MenuEntity> menusById;
    private Collection<NhanvienEntity> nhanviensById;
    private Collection<ThongtinbanEntity> thongtinbansById;

    @Id
    @Column(name = "ID", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "Ten", nullable = true, length = 100)
    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    @Basic
    @Column(name = "DiaChi", nullable = true, length = 100)
    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    @Basic
    @Column(name = "SDT", nullable = true, length = 15)
    public String getSdt() {
        return sdt;
    }

    public void setSdt(String sdt) {
        this.sdt = sdt;
    }

    @Basic
    @Column(name = "TinhThanh", nullable = true, length = 100)
    public String getTinhThanh() {
        return tinhThanh;
    }

    public void setTinhThanh(String tinhThanh) {
        this.tinhThanh = tinhThanh;
    }

    @Basic
    @Column(name = "SoLuongBan", nullable = true)
    public Integer getSoLuongBan() {
        return soLuongBan;
    }

    public void setSoLuongBan(Integer soLuongBan) {
        this.soLuongBan = soLuongBan;
    }

    @Basic
    @Column(name = "ID_QuanLy", nullable = true)
    public Integer getIdQuanLy() {
        return idQuanLy;
    }

    public void setIdQuanLy(Integer idQuanLy) {
        this.idQuanLy = idQuanLy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChinhanhEntity that = (ChinhanhEntity) o;

        if (id != that.id) return false;
        if (ten != null ? !ten.equals(that.ten) : that.ten != null) return false;
        if (diaChi != null ? !diaChi.equals(that.diaChi) : that.diaChi != null) return false;
        if (sdt != null ? !sdt.equals(that.sdt) : that.sdt != null) return false;
        if (tinhThanh != null ? !tinhThanh.equals(that.tinhThanh) : that.tinhThanh != null) return false;
        if (soLuongBan != null ? !soLuongBan.equals(that.soLuongBan) : that.soLuongBan != null) return false;
        if (idQuanLy != null ? !idQuanLy.equals(that.idQuanLy) : that.idQuanLy != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (ten != null ? ten.hashCode() : 0);
        result = 31 * result + (diaChi != null ? diaChi.hashCode() : 0);
        result = 31 * result + (sdt != null ? sdt.hashCode() : 0);
        result = 31 * result + (tinhThanh != null ? tinhThanh.hashCode() : 0);
        result = 31 * result + (soLuongBan != null ? soLuongBan.hashCode() : 0);
        result = 31 * result + (idQuanLy != null ? idQuanLy.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "chinhanhByIdChiNhanh")
    public Collection<ChiphiEntity> getChiphisById() {
        return chiphisById;
    }

    public void setChiphisById(Collection<ChiphiEntity> chiphisById) {
        this.chiphisById = chiphisById;
    }

    @OneToMany(mappedBy = "chinhanhByIdChiNhanh")
    public Collection<DonhangEntity> getDonhangsById() {
        return donhangsById;
    }

    public void setDonhangsById(Collection<DonhangEntity> donhangsById) {
        this.donhangsById = donhangsById;
    }

    @OneToMany(mappedBy = "chinhanhByIdChiNhanh")
    public Collection<MenuEntity> getMenusById() {
        return menusById;
    }

    public void setMenusById(Collection<MenuEntity> menusById) {
        this.menusById = menusById;
    }

    @OneToMany(mappedBy = "chinhanhByIdChiNhanh")
    public Collection<NhanvienEntity> getNhanviensById() {
        return nhanviensById;
    }

    public void setNhanviensById(Collection<NhanvienEntity> nhanviensById) {
        this.nhanviensById = nhanviensById;
    }

    @OneToMany(mappedBy = "chinhanhByIdChiNhanh")
    public Collection<ThongtinbanEntity> getThongtinbansById() {
        return thongtinbansById;
    }

    public void setThongtinbansById(Collection<ThongtinbanEntity> thongtinbansById) {
        this.thongtinbansById = thongtinbansById;
    }
}
