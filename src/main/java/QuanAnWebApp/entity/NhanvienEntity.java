package QuanAnWebApp.entity;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "nhanvien", schema = "quanlycuahang", catalog = "")
public class NhanvienEntity {
    private int id;
    private String taiKhoan;
    private String matKhau;
    private String tenNhanVien;
    private String diaChi;
    private String sdt;
    private String email;
    private Integer chucVu;
    private Timestamp ngayVao;
    private ChinhanhEntity chinhanhByIdChiNhanh;

    @Id
    @Column(name = "ID", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "TaiKhoan", nullable = true, length = 50)
    public String getTaiKhoan() {
        return taiKhoan;
    }

    public void setTaiKhoan(String taiKhoan) {
        this.taiKhoan = taiKhoan;
    }

    @Basic
    @Column(name = "MatKhau", nullable = true, length = 255)
    public String getMatKhau() {
        return matKhau;
    }

    public void setMatKhau(String matKhau) {
        this.matKhau = matKhau;
    }

    @Basic
    @Column(name = "TenNhanVien", nullable = true, length = 100)
    public String getTenNhanVien() {
        return tenNhanVien;
    }

    public void setTenNhanVien(String tenNhanVien) {
        this.tenNhanVien = tenNhanVien;
    }

    @Basic
    @Column(name = "DiaChi", nullable = true, length = 300)
    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    @Basic
    @Column(name = "SDT", nullable = true, length = 20)
    public String getSdt() {
        return sdt;
    }

    public void setSdt(String sdt) {
        this.sdt = sdt;
    }

    @Basic
    @Column(name = "Email", nullable = true, length = 100)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "ChucVu", nullable = true)
    public Integer getChucVu() {
        return chucVu;
    }

    public void setChucVu(Integer chucVu) {
        this.chucVu = chucVu;
    }

    @Basic
    @Column(name = "NgayVao", nullable = true)
    public Timestamp getNgayVao() {
        return ngayVao;
    }

    public void setNgayVao(Timestamp ngayVao) {
        this.ngayVao = ngayVao;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NhanvienEntity that = (NhanvienEntity) o;

        if (id != that.id) return false;
        if (taiKhoan != null ? !taiKhoan.equals(that.taiKhoan) : that.taiKhoan != null) return false;
        if (matKhau != null ? !matKhau.equals(that.matKhau) : that.matKhau != null) return false;
        if (tenNhanVien != null ? !tenNhanVien.equals(that.tenNhanVien) : that.tenNhanVien != null) return false;
        if (diaChi != null ? !diaChi.equals(that.diaChi) : that.diaChi != null) return false;
        if (sdt != null ? !sdt.equals(that.sdt) : that.sdt != null) return false;
        if (email != null ? !email.equals(that.email) : that.email != null) return false;
        if (chucVu != null ? !chucVu.equals(that.chucVu) : that.chucVu != null) return false;
        if (ngayVao != null ? !ngayVao.equals(that.ngayVao) : that.ngayVao != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (taiKhoan != null ? taiKhoan.hashCode() : 0);
        result = 31 * result + (matKhau != null ? matKhau.hashCode() : 0);
        result = 31 * result + (tenNhanVien != null ? tenNhanVien.hashCode() : 0);
        result = 31 * result + (diaChi != null ? diaChi.hashCode() : 0);
        result = 31 * result + (sdt != null ? sdt.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (chucVu != null ? chucVu.hashCode() : 0);
        result = 31 * result + (ngayVao != null ? ngayVao.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "ID_ChiNhanh", referencedColumnName = "ID")
    public ChinhanhEntity getChinhanhByIdChiNhanh() {
        return chinhanhByIdChiNhanh;
    }

    public void setChinhanhByIdChiNhanh(ChinhanhEntity chinhanhByIdChiNhanh) {
        this.chinhanhByIdChiNhanh = chinhanhByIdChiNhanh;
    }
}
