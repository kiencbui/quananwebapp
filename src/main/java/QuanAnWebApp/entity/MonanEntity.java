package QuanAnWebApp.entity;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "monan", schema = "quanlycuahang", catalog = "")
public class MonanEntity {
    private int id;
    private String ten;
    private Integer gia;
    private String thongTin;
    private String hinhAnh;
    private Collection<ChitietdonhangEntity> chitietdonhangsById;
    private Collection<GiohangEntity> giohangsById;
    private Collection<MenuEntity> menusById;
    private DanhmucmonanEntity danhmucmonanByIdDanhMuc;

    @Id
    @Column(name = "ID", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "Ten", nullable = true, length = 100)
    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    @Basic
    @Column(name ="HinhAnh", nullable = false, length = 300)
    public String getHinhAnh() {
        return hinhAnh;
    }

    public void setHinhAnh(String hinhAnh) {
        this.hinhAnh = hinhAnh;
    }

    @Basic
    @Column(name = "Gia", nullable = true)
    public Integer getGia() {
        return gia;
    }

    public void setGia(Integer gia) {
        this.gia = gia;
    }

    @Basic
    @Column(name = "ThongTin", nullable = true, length = 300)
    public String getThongTin() {
        return thongTin;
    }

    public void setThongTin(String thongTin) {
        this.thongTin = thongTin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MonanEntity that = (MonanEntity) o;

        if (id != that.id) return false;
        if (ten != null ? !ten.equals(that.ten) : that.ten != null) return false;
        if (gia != null ? !gia.equals(that.gia) : that.gia != null) return false;
        if (thongTin != null ? !thongTin.equals(that.thongTin) : that.thongTin != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (ten != null ? ten.hashCode() : 0);
        result = 31 * result + (gia != null ? gia.hashCode() : 0);
        result = 31 * result + (thongTin != null ? thongTin.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "monanByIdMonAn")
    public Collection<ChitietdonhangEntity> getChitietdonhangsById() {
        return chitietdonhangsById;
    }

    public void setChitietdonhangsById(Collection<ChitietdonhangEntity> chitietdonhangsById) {
        this.chitietdonhangsById = chitietdonhangsById;
    }

    @OneToMany(mappedBy = "monanByIdMonAn")
    public Collection<GiohangEntity> getGiohangsById() {
        return giohangsById;
    }

    public void setGiohangsById(Collection<GiohangEntity> giohangsById) {
        this.giohangsById = giohangsById;
    }

    @OneToMany(mappedBy = "monanByIdMonAn")
    public Collection<MenuEntity> getMenusById() {
        return menusById;
    }

    public void setMenusById(Collection<MenuEntity> menusById) {
        this.menusById = menusById;
    }

    @ManyToOne
    @JoinColumn(name = "ID_DanhMuc", referencedColumnName = "ID")
    public DanhmucmonanEntity getDanhmucmonanByIdDanhMuc() {
        return danhmucmonanByIdDanhMuc;
    }

    public void setDanhmucmonanByIdDanhMuc(DanhmucmonanEntity danhmucmonanByIdDanhMuc) {
        this.danhmucmonanByIdDanhMuc = danhmucmonanByIdDanhMuc;
    }
}
