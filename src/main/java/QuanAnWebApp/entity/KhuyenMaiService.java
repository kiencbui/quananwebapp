package QuanAnWebApp.entity;

import QuanAnWebApp.dao.KhuyenMaiDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("KhuyenMaiService")
@Transactional
public class KhuyenMaiService {

    @Autowired
    private KhuyenMaiDao dao;

    public void insertSubscribe(DangkiKmEntity km) {
        dao.insertSubscribe(km);
    }
}
