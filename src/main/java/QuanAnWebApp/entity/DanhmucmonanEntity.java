package QuanAnWebApp.entity;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "danhmucmonan", schema = "quanlycuahang", catalog = "")
public class DanhmucmonanEntity {
    private int id;
    private String tenDanhMuc;
    private Collection<MonanEntity> monansById;

    @Id
    @Column(name = "ID", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "TenDanhMuc", nullable = true, length = 100)
    public String getTenDanhMuc() {
        return tenDanhMuc;
    }

    public void setTenDanhMuc(String tenDanhMuc) {
        this.tenDanhMuc = tenDanhMuc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DanhmucmonanEntity that = (DanhmucmonanEntity) o;

        if (id != that.id) return false;
        if (tenDanhMuc != null ? !tenDanhMuc.equals(that.tenDanhMuc) : that.tenDanhMuc != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (tenDanhMuc != null ? tenDanhMuc.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "danhmucmonanByIdDanhMuc")
    public Collection<MonanEntity> getMonansById() {
        return monansById;
    }

    public void setMonansById(Collection<MonanEntity> monansById) {
        this.monansById = monansById;
    }
}
