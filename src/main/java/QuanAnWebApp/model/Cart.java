package QuanAnWebApp.model;

import QuanAnWebApp.entity.MonanEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import java.util.HashMap;

@Component
@SessionScope
public class Cart {

    HashMap<MonanEntity,Integer> dsMonAn;

    public HashMap<MonanEntity, Integer> getDsMonAn() {
        if(dsMonAn == null)
            dsMonAn = new HashMap<MonanEntity,Integer>();
        return dsMonAn;
    }

    public void setDsMonAn(HashMap<MonanEntity, Integer> dsMonAn) {
        this.dsMonAn = dsMonAn;
    }
}
