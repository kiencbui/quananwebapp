package QuanAnWebApp.model;

import QuanAnWebApp.entity.DonhangEntity;
import QuanAnWebApp.entity.KhachhangEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

@Component
@SessionScope
public class UserLogin {

    KhachhangEntity userLogin;

    DonhangEntity donhangEntity;

    CheckoutForm checkoutForm;

    public DonhangEntity getDonhangEntity() {
        return donhangEntity;
    }

    public void setDonhangEntity(DonhangEntity donhangEntity) {
        this.donhangEntity = donhangEntity;
    }

    public CheckoutForm getCheckoutForm() {
        return checkoutForm;
    }

    public void setCheckoutForm(CheckoutForm checkoutForm) {
        this.checkoutForm = checkoutForm;
    }

    public KhachhangEntity getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(KhachhangEntity userLogin) {
        this.userLogin = userLogin;
    }
}
