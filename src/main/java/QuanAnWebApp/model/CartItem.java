package QuanAnWebApp.model;

import QuanAnWebApp.entity.MonanEntity;

public class CartItem {
    MonanEntity monAn;
    Integer soLuong;

    public MonanEntity getMonAn() {
        return monAn;
    }

    public void setMonAn(MonanEntity monAn) {
        this.monAn = monAn;
    }

    public Integer getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(Integer soLuong) {
        this.soLuong = soLuong;
    }
}
