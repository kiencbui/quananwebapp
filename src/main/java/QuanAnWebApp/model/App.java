package QuanAnWebApp.model;

import QuanAnWebApp.entity.ChinhanhEntity;
import QuanAnWebApp.entity.DanhmucmonanEntity;
import QuanAnWebApp.service.ChiNhanhService;
import QuanAnWebApp.service.DanhMucService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.LinkedList;
import java.util.List;

@Component
public class App {

    @Autowired
    private DanhMucService danhMucService;

    @Autowired
    private ChiNhanhService chiNhanhService;

    private static int visitedCount = 0;
    public static List<DanhmucmonanEntity> dsDanhMuc = new LinkedList<>();
    public static List<ChinhanhEntity> dsChiNhanh = new LinkedList<>();

    public int getVisitedCount() {
        return ++visitedCount;
    }

    public List<DanhmucmonanEntity> getDsDanhMuc() {
        return dsDanhMuc;
    }

    public List<ChinhanhEntity> getDsChiNhanh() {
        return dsChiNhanh;
    }

    @PostConstruct
    public void init() {
        dsChiNhanh = chiNhanhService.findAllChiNhanh();
        dsDanhMuc = danhMucService.findAllDanhMuc();
    }
}
