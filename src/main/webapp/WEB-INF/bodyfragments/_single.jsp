<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="mens">
    <div class="main">
        <div class="wrap">
            <ul class="breadcrumb breadcrumb__t"><a class="home" href="#">Home</a> / <a href="#">Dishes</a> / Detail</ul>
            <div class="cont span_2_of_3">
                <div class="grid images_3_of_2">
                    <ul id="etalage">
                        <li>
                            <a href="#">
                                <img class="etalage_thumb_image" src="${monAn.hinhAnh}" class="img-responsive" />
                            </a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="desc1 span_3_of_2">
                    <h3 class="m_3">${monAn.ten}</h3>
                    <p class="m_5">$${monAn.gia}</p>
                    <div class="btn_form">
                        <form>
                            <span>Quantity: </span>
                            <input id="soLuong" type="number" min="1" value="1" title="">
                            <input id="btnAddCart" type="submit" value="buy" title="">
                        </form>
                    </div>
                    <span class="m_link"> </span>
                    <p class="m_text2">${monAn.thongTin}</p>
                </div>
                <div class="clear"></div>
                <div class="clients">
                    <h3 class="m_3">10 Other Products in the same category</h3>
                    <ul id="flexiselDemo3">
                        <c:forEach items="${relative}" var="item">
                            <li><img src="${item.hinhAnh}" /><a href="${pageContext.request.contextPath}/detail/${item.id}">${item.ten}</a><p>$${item.gia}</p></li>
                        </c:forEach>
                    </ul>
                    <script type="text/javascript">
                        $(window).load(function() {

                            $("#flexiselDemo1").flexisel();
                            $("#flexiselDemo2").flexisel({
                                enableResponsiveBreakpoints: true,
                                responsiveBreakpoints: {
                                    portrait: {
                                        changePoint:480,
                                        visibleItems: 1
                                    },
                                    landscape: {
                                        changePoint:640,
                                        visibleItems: 2
                                    },
                                    tablet: {
                                        changePoint:768,
                                        visibleItems: 3
                                    }
                                }
                            });

                            $("#flexiselDemo3").flexisel({
                                visibleItems: 5,
                                animationSpeed: 1000,
                                autoPlay: false,
                                autoPlaySpeed: 3000,
                                pauseOnHover: true,
                                enableResponsiveBreakpoints: true,
                                responsiveBreakpoints: {
                                    portrait: {
                                        changePoint:480,
                                        visibleItems: 1
                                    },
                                    landscape: {
                                        changePoint:640,
                                        visibleItems: 2
                                    },
                                    tablet: {
                                        changePoint:768,
                                        visibleItems: 3
                                    }
                                }
                            });

                            $('form').submit(false);

                            $('#btnAddCart').click(function () {
                                $.post("${pageContext.request.contextPath}/api/addToCart",
                                    {
                                        idMonAn: "${monAn.id}",
                                        soLuong: $('#soLuong').val()
                                    },
                                    function(data,status){
                                    if(data=="done")
                                        alert("Added to Cart");
                                    });
                            });


                        });


                    </script>
                    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.flexisel.js"></script>
                </div>
                <div class="toogle">
                    <h3 class="m_3">Product Details</h3>
                    <p class="m_text">${monAn.thongTin}</p>
                </div>
            </div>
            <div class="rsingle span_1_of_single">
                <h5 class="m_1">Categories</h5>
                <ul class="kids">
                    <c:forEach items="${app.dsDanhMuc}" var="dm">
                        <li><a href="${pageContext.request.contextPath}/category/${dm.id}">${dm.tenDanhMuc}</a></li>
                    </c:forEach>
                </ul>
                <script src="${pageContext.request.contextPath}/js/jquery.easydropdown.js"></script>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
</div>