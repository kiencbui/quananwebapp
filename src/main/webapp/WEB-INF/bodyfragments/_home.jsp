<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="main">
    <div class="wrap">
        <div class="section group">
            <div class="cont span_2_of_3">
                <h2 class="head">Featured Products </h2>
                <div class="top-box">
                    <c:forEach items="${topMonAn}" var="monAn">
                        <div class="col_1_of_3 span_1_of_3">
                            <a href="${pageContext.request.contextPath}/detail/${monAn.id}">
                                <div class="inner_content clearfix">
                                    <div class="product_image">
                                        <img src="${monAn.hinhAnh}" alt=""/>
                                    </div>
                                    <div class="sale-box1"><span class="on_sale title_shop">Sale</span></div>
                                    <div class="price">
                                        <div class="cart-left">
                                            <p class="title">${monAn.ten}</p>
                                            <div class="price1">
                                                <span class="actual">$${monAn.gia}</span>
                                            </div>
                                        </div>
                                        <div class="cart-right"> </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </c:forEach>
                    <div class="clear"></div>
                </div>

                <h2 class="head">New Products</h2>
                <div class="section group">
                    <c:forEach items="${newMonAn}" var="monAn">
                        <div class="col_1_of_3 span_1_of_3">
                            <a href="${pageContext.request.contextPath}/detail/${monAn.id}">
                                <div class="inner_content clearfix">
                                    <div class="product_image">
                                        <img src="${monAn.hinhAnh}" alt=""/>
                                    </div>
                                    <div class="sale-box"><span class="on_sale title_shop">New</span></div>
                                    <div class="price">
                                        <div class="cart-left">
                                            <p class="title">${monAn.ten}</p>
                                            <div class="price1">
                                                <span class="actual">$ ${monAn.gia}</span>
                                            </div>
                                        </div>
                                        <div class="cart-right"> </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </c:forEach>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="rsidebar span_1_of_left">

                <div class="top-border"> </div>
                <div class="sidebar-bottom">
                    <h2 class="m_1">Newsletters<br> Signup</h2>
                    <p class="m_text">Lorem ipsum dolor sit amet, consectetuer</p>
                    <div class="subscribe">
                        <form>
                            <input id="email" type="text" class="textbox">
                            <input id="subscribe" type="submit" value="Subscribe">
                        </form>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(window).load(function() {

        $('form').submit(false);

        $('#subscribe').click(function () {
            $.post("${pageContext.request.contextPath}/api/subscribe",
                {
                    email: $('#email').val()
                },
                function(data,status){
                    if(data=="done")
                        alert("Subscribed");
                });
        });

    });
</script>