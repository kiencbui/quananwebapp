<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="main">
    <div class="wrap">
        <div class="section group">
            <div class="cont span_2_of_3">
                <h2 class="head">Dishes</h2>
                <c:forEach begin="0" end="${dsMonAn.size()/3}" varStatus="loop">
                    <c:set var="index" value="${loop.index*3}"/>
                    <c:choose>
                        <c:when test="${dsMonAn.size()>=index+3}">
                            <c:set var="sublist" value="${dsMonAn.subList(index,index+3)}"/>
                        </c:when>
                        <c:when test="${dsMonAn.size()<index+3}">
                            <c:set var="sublist" value="${dsMonAn.subList(index,dsMonAn.size())}"/>
                        </c:when>
                    </c:choose>
                    
                    <div class="top-box">
                        <c:forEach items="${sublist}" var="item">
                            <div class="col_1_of_3 span_1_of_3">
                                <a href="${pageContext.request.contextPath}/detail/${item.id}">
                                    <div class="inner_content clearfix">
                                        <div class="product_image">
                                            <img src="${item.hinhAnh}" alt=""/>
                                        </div>
                                        <div class="price">
                                            <div class="cart-left">
                                                <p class="title">${item.ten}</p>
                                                <div class="price1">
                                                    <span class="actual">$${item.gia}</span>
                                                </div>
                                            </div>
                                            <div class="cart-right"> </div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </c:forEach>
                        <div class="clear"></div>
                    </div>
                </c:forEach>
            </div>
            <div class="rsidebar span_1_of_left">

                <h5 class="m_1">Categories</h5>

                <ul class="kids">
                    <c:forEach items="${app.dsDanhMuc}" var="dm">
                        <li><a href="${pageContext.request.contextPath}/category/${dm.id}">${dm.tenDanhMuc}</a></li>
                    </c:forEach>
                </ul>

            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>