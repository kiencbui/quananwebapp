<div class="register_account">
    <div class="wrap">
        <h4 class="title">Create an Account</h4>
        <form action="" method="post" name="register" id="register-form">
            <div class="col_1_of_2 span_1_of_2">
                <div><input name="taiKhoan" type="text" placeholder="Username" value="" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = '';}"></div>
                <div><input name="matKhau" type="text" placeholder="Password" value="" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = '';}"></div>
                <div><input name="tenKhachHang" type="text" placeholder="Full Name" value="" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = '';}"></div>
            </div>
            <div class="col_1_of_2 span_1_of_2">
                <div><input name="sdt" type="text" placeholder="Phone number" value="" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = '';}"></div>
                <div><input name="email" type="text" placeholder="Email" value="" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = '';}"></div>
                <div><input name="diaChi" type="text" placeholder="Address" value="" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = '';}"></div>
            </div>
            <button type="submit" class="grey">Submit</button>
            <p class="terms" style="color: red">${error}</p>
            <div class="clear"></div>
        </form>
    </div>
</div>