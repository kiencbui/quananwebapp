<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<style>
    table.child {
        font-family: "Arial", Times, sans-serif;
        border: 1px solid #FFFFFF;
        width: 100%;
        height: 100%;
        text-align: center;
        border-collapse: collapse;
    }

    table.paleBlueRows {
        font-family: "Arial", Times, sans-serif;
        border: 1px solid #FFFFFF;
        width: 100%;
        height: 200px;
        text-align: center;
        border-collapse: collapse;
    }
    table.paleBlueRows td, table.paleBlueRows th {
        border: 1px solid #FFFFFF;
        padding: 3px 2px;
    }
    table.paleBlueRows tbody td {
        font-size: 13px;
    }
    table.paleBlueRows tr:nth-child(even) {
        background: #D0E4F5;
    }
    table.paleBlueRows thead {
        background: #0B6FA4;
        border-bottom: 5px solid #FFFFFF;
    }
    table.paleBlueRows thead th {
        font-size: 17px;
        font-weight: bold;
        color: #FFFFFF;
        text-align: center;
        border-left: 2px solid #FFFFFF;
    }
    table.paleBlueRows thead th:first-child {
        border-left: none;
    }

    table.paleBlueRows tfoot {
        font-size: 14px;
        font-weight: bold;
        color: #333333;
        background: #D0E4F5;
        border-top: 3px solid #444444;
    }
    table.paleBlueRows tfoot td {
        font-size: 14px;
    }
</style>
<div class="login">
    <div class="wrap">
        <h5 class="m_6">Detail</h5>

        <div class="m_text">
            <table class="paleBlueRows">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Detail Checkout</th>
                    <th>Address</th>
                    <th>Type</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>

                <c:forEach items="${dsDonHang}" var="item">
                    <tr>
                        <td>${item.id}</td>
                        <td>

                            <table class="child">
                                <c:forEach items="${item.chitietdonhangsById}" var="detail">
                                    <tr>
                                        <td>${detail.monanByIdMonAn.ten}</td>
                                        <td>$${detail.monanByIdMonAn.gia}</td>
                                        <td>${detail.soLuong}</td>
                                    </tr>
                                    <c:set var="total" value="${total + detail.monanByIdMonAn.gia*detail.soLuong}"/>
                                </c:forEach>
                                <tr style="font-weight: bold">
                                    <td >Total: $${total}</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>
                            <c:set var="total" value="0"/>

                        </td>
                        <td>${item.diaChiGiaoHang}</td>
                        <td>
                            <c:choose>
                                <c:when test="${item.loaiDonHang==0}">
                                    Dung tai quan
                                </c:when>
                                <c:when test="${item.loaiDonHang==1}">
                                    Mang ve
                                </c:when>
                                <c:when test="${item.loaiDonHang==2}">
                                    Online
                                </c:when>
                            </c:choose>
                        </td>
                        <td>
                            <c:choose>
                                <c:when test="${item.tinhTrang==0}">
                                    Vo Hieu
                                </c:when>
                                <c:when test="${item.tinhTrang==1}">
                                    Da hoan thanh
                                </c:when>
                                <c:when test="${item.tinhTrang==2}">
                                    Dang cho duyet
                                </c:when>
                                <c:when test="${item.tinhTrang==3}">
                                    Da duoc duyet
                                </c:when>
                                <c:when test="${item.tinhTrang==4}">
                                    Dang van chuyen
                                </c:when>
                                <c:when test="${item.tinhTrang==5}">
                                    Da hoan thanh
                                </c:when>
                            </c:choose>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>