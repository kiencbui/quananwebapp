<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<style>
    table.child {
        font-family: "Arial", Times, sans-serif;
        border: 1px solid #FFFFFF;
        width: 100%;
        height: 100%;
        text-align: center;
        border-collapse: collapse;
    }

    table.paleBlueRows {
        font-family: "Arial", Times, sans-serif;
        border: 1px solid #FFFFFF;
        width: 100%;
        height: 100px;
        text-align: center;
        border-collapse: collapse;
    }
    table.paleBlueRows td, table.paleBlueRows th {
        border: 1px solid #FFFFFF;
        padding: 3px 2px;
    }
    table.paleBlueRows tbody td {
        font-size: 13px;
    }
    table.paleBlueRows tr:nth-child(even) {
        background: #D0E4F5;
    }
    table.paleBlueRows thead {
        background: #0B6FA4;
        border-bottom: 5px solid #FFFFFF;
    }
    table.paleBlueRows thead th {
        font-size: 17px;
        font-weight: bold;
        color: #FFFFFF;
        text-align: center;
        border-left: 2px solid #FFFFFF;
    }
    table.paleBlueRows thead th:first-child {
        border-left: none;
    }

    table.paleBlueRows tfoot {
        font-size: 14px;
        font-weight: bold;
        color: #333333;
        background: #D0E4F5;
        border-top: 3px solid #444444;
    }
    table.paleBlueRows tfoot td {
        font-size: 14px;
    }
</style>
<div class="login">
    <div class="wrap">
        <h5 class="m_6">Checkout</h5>

        <div class="m_text">
            <table class="paleBlueRows">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Product Name</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Edit</th>
                </tr>
                </thead>
                <tbody>
                    <c:forEach items="${dsMonAn}" var="item">
                        <tr>
                            <td>${item.monAn.id}</td>
                            <td>${item.monAn.ten}</td>
                            <td>$${item.monAn.gia}</td>
                            <td>${item.soLuong}</td>
                            <td>
                                <span><a href="${pageContext.request.contextPath}/detail/${item.monAn.id}">Edit</a></span>
                                <span><a href="${pageContext.request.contextPath}/cart/remove/${item.monAn.id}">Remove</a></span>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
            <br>
            <br>
            <div class="btn_form" style="text-align: center; display: inline">
                <form action="${pageContext.request.contextPath}/checkout">
                    <div>
                        <span>Address: </span>
                        <input name="address" type="text" value="" placeholder="Address">

                        <span>Note: </span>
                        <input name="note" type="text" value="" placeholder="Note">

                        <input type="submit" value="CHECKOUT" title="">
                    </div>
                    <br>
                </form>
            </div>
        </div>
    </div>
</div>