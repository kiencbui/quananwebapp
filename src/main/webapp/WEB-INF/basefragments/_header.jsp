<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--<jsp:useBean id="app" class="QuanAnWebApp.model.App"/>--%>
<div class="header-top">
    <div class="wrap">
        <div class="header-top-left">

            <div class="clear"></div>
        </div>
        <div class="cssmenu">
            <ul>
                <li class="active"><a href="${pageContext.request.contextPath}/account">Account</a></li> |
                <li id="login-menu"><a href="${pageContext.request.contextPath}/login">Log In</a></li> |
                <li id="signup-menu"><a href="${pageContext.request.contextPath}/register">Sign Up</a></li> |
                <li id="signout-menu"><a href="${pageContext.request.contextPath}/logout">Sign Out</a></li>
            </ul>
        </div>
        <div class="clear"></div>
    </div>
</div>
<div class="header-bottom">
    <div class="wrap">
        <div class="header-bottom-left">
            <div class="logo">
                <a href="${pageContext.request.contextPath}/"><img src="${pageContext.request.contextPath}/images/logo.png" alt=""/></a>
            </div>
            <div class="menu">
                <ul class="megamenu skyblue">
                    <li class="active grid"><a href="${pageContext.request.contextPath}/">Home</a></li>
                    <li><a class="color4" href="${pageContext.request.contextPath}/category/">Categories</a>
                        <div class="megapanel">
                            <div class="row">
                                <div class="col1">
                                    <div class="h_nav">
                                        <h4>
                                            <ul>
                                                <c:forEach items="${app.dsDanhMuc}" var="cat">
                                                    <li><a href="${pageContext.request.contextPath}/category/${cat.id}"><c:out value="${cat.tenDanhMuc}"/></a></li>
                                                </c:forEach>
                                            </ul>
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li><a class="color5" href="${pageContext.request.contextPath}/branch/">Branches</a>
                        <div class="megapanel">
                            <div class="col1">
                                <div class="h_nav">
                                    <%--<h4>Danh Sách Chi Nhánh</h4>--%>
                                    <h4>
                                        <ul>
                                            <c:forEach items="${app.dsChiNhanh}" var="branch">
                                                <li><a href="${pageContext.request.contextPath}/branch/${branch.id}"><c:out value="${branch.ten}"/></a></li>
                                            </c:forEach>
                                        </ul>
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li><a class="color6" href="${pageContext.request.contextPath}/about/">About Us</a></li>
                </ul>
            </div>
        </div>
        <div class="header-bottom-right">
            <div class="search">
                <input type="text" name="s" id="s" class="textbox" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Keyword';}">
                <input type="submit" value="Search" id="submit" name="submit" onclick="window.location.href='${pageContext.request.contextPath}/search/'+$('#s').val()">
                <div id="response"> </div>
            </div>
            <div class="tag-list">
                <ul class="icon1 sub-icon1 profile_img">
                    <li><a class="active-icon c1" href="#"> </a>
                        <ul class="sub-icon1 list">
                            <li><h3>sed diam nonummy</h3><a href=""></a></li>
                            <li><p>Lorem ipsum dolor sit amet, consectetuer  <a href="">adipiscing elit, sed diam</a></p></li>
                        </ul>
                    </li>
                </ul>
                <ul class="icon1 sub-icon1 profile_img">
                    <li><a class="active-icon c2" href="#"> </a>
                        <ul class="sub-icon1 list">
                            <li><h3>No Products</h3><a href=""></a></li>
                            <li><p>Lorem ipsum dolor sit amet, consectetuer  <a href="">adipiscing elit, sed diam</a></p></li>
                        </ul>
                    </li>
                </ul>
                <ul class="last"><li><a href="${pageContext.request.contextPath}/cart">Cart</a></li></ul>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>